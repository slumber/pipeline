
## TP0 : The tour 🏗️

---

##  Initial Setup

<img src="attachements/module_5-kapla-init.png" width=1000>

---

### Goal

*Collaborate to assemble available parts to get the <font color="orange">**highest** **stable**</font> structure possible in 15 minutes*

---

<img src="attachements/module_5-kapla-try.png" width=1000>

---

### Rules

*Only <font color="orange">translate</font> and <font color="orange">rotate</font> transformations are allowed !*

---

<img src="attachements/module_5-kapla-simulation.gif" width=1000>

---

### Advices 

- Choose a member as coordinator
- Talk together ^^

---

## Questions ?

<!-- %% TOOGLE RIGID BODIES
```python
import bpy

for o in bpy.data.objects:
    o.select_set(True)
    with bpy.context.temp_override(object=o):
        bpy.ops.rigidbody.object_add()
        o.rigid_body.collision_shape = 'BOX'

        if "Cube" in o.name:
            o.rigid_body.type = 'ACTIVE'
        else:
            o.rigid_body.type = 'PASSIVE'
```
Blend for session: [[attachements/module_5-kapla_startup_scene_solo.blend]]
%% -->