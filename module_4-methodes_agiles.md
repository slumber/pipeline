# Les méthodes agiles pour la production d’image de synthèse

notes:
Je vous donne ce cours parce que :
-   Frustré d’arriver dans le milieu professionnel sans connaitre ça ! C’est utilisé dans les équipes R&D.
-   Ça peut avoir des applications très concrètes dans les productions !
-   Pour vous ouvrir sur de nouvelles façons d’envisager une production. Plus humaines.
-  Méthodes utilisées pour la  collaboration temps réel

---

<!-- .slide: data-stack-name="Agile ?" -->

## Méthode agile ?

![](attachements/module_4-what_is_that.gif)

notes:
Dans un premier temps on va explorer le concept general. L’idée c’est de tailler en V, de rentrer progressivement dans les détails.

--

### Pourquoi ?

notes:
Dans un premier temps on va voir qu’est-ce qui a motivé l’apparition des méthodes agiles.


--

![](attachements/module_4-back_to_the_future.gif)

notes:
Pour comprendre ces méthodes il faut faire un bon dans le passé… Afin de connaitre les éléments qui ont motivés leur apparition.

--

![](attachements/module_4-ww1.gif)

notes:
Tout à commencé aux alentour de la première guerre mondiale.
Vous allez me dire, quel et le rapport avec la choucroute ? Et bien durant cette période, un certain…

--

![](attachements/module_4-gantt.jpg)

Henry Laurence Gantt

notes:
Henry Laurence Gantt, créée un outil pour faciliter la gestion de production… -->

[Plus sur l’histoire de gantt](https://www.onepager.com/community/blog/a-brief-history-of-the-gantt-chart/)

--

![](attachements/module_4-ganttold.png)

Le diagramme de Gantt

notes:
Le fameux diagramme de Gantt !
Des gens parmit vous en ont déjà entendu parler ?

--

![](attachements/module_4-ww1tank.gif)

Usine d’assemblage de tank.

notes:
lors de la première guerre mondiale, un certain William Crozier, alors général dans l'armée américaine a trouvé l'outil genial et a poussé pour qu'ilIl soit utilisé pour gérer la production de de l'arsenal, et notamment des avions.

Il trouvait l’outil particulièrement utile car il permettait de voir quand une action était necessaire pour ajuster la production aux objectifs.

Suite à la pub faite par le général pendant la guerre, la méthode à été adoptés dans divers institutions publiques puis dans la majorités des industries.

--

![](attachements/module_4-waterfall.gif)

notes:
Depuis cette époque de nombreuses industrie se basent sur cela pour gérer leur production. C’est la base de la méthode dite “waterfall” (ou cascade).

**Mais qu’est-ce que c’est “waterfall” ?**

--

![](attachements/module_4-waterfall_dev.png)

Cycle de développement logiciel en “cascade”

notes:
Plus concrètement c’est une méthode qui consiste à diviser la production en une succession d’étape linéaire et interdépendantes.

Un exemple avec le développement logiciel.

**Soit dit en passant ça vous rappel pas quelque chose ? ^^**

--

<img src='attachements/module_4-animation_workflow6.svg' width=1000>

notes:
Et oui nos productions actuelles d’animation s’appuient sur des méthodes utilisés et initiée durant la première guerre mondiale :o

**Mais**

-   Est-ce une bonne chose ?
-   Est-ce que cette méthode est efficace ?

--

### Avantages

-   Cahier des charge et budget clairs<!-- .element: class="fragment" -->
-   Modèle éprouvé<!-- .element: class="fragment" -->

notes:
Ça avait quelques avantages :

-   Cahier des charges et budget clairs
-   Modèle éprouvé: on sait à quoi s’attendre
-   En général, respect scrupuleux du cahier des charges

Mais ça à aussi pas mal d’inconvénients.

--

### Inconvénients

-   Pas de place pour les imprévus<!-- .element: class="fragment" -->
-   Rigidité face aux changements<!-- .element: class="fragment" -->
-   Ajustements post-livraison coûteux<!-- .element: class="fragment" -->

notes:
-   Peu ou pas de place pour les imprévus
-   Rigidité face aux changements
-   Ajustements post-livraison coûteux

Chaque partie section à une date, et lorsque les dates sont dépassé ça décalle TOUT

--

![](attachements/module_4-expectationVSreality.jpg)

notes:
-   Risque de déphasage entre le moment de l’analyse du besoin et la livraison finale

Mais comment tenter de résoudre ces soucis qui résident aux fondements de nos méthodes de travail actuelles ?

---

### Méthodes agiles

notes:
Parmis toutes les discipline c’est bien le développement logiciel qui souffraient le plus de ces méthodes. C’est donc pas étonnant que ce soit de ce milieu qu’émerge une solution au soucis

Du coup c’est répondre à ces problèmes que les méthodes Agiles sont apparues.

Sachez que tout ce qu’on va voir est transposable à d’autres disciplines !

--

![|x600](attachements/module_4-round_table.jpg)

notes:
Les concepts agiles ont étés formalisés dans un Manifeste “Agile” en 2001 au États-Unis par un groupe de 17 experts en développement logiciel(je ne donnerai pas les noms mais on les trouve facilement sur internet). Ils avaient tous expérimenté des méthodes plus légères chacun dans leur coin.

Ce manifeste a pour but de définir les dénominateurs communs aux différentes méthodes que les membres avaient expérimentées.

_Les méthodes agiles ne sont donc pas apparues avec le manifeste agile, elles étaients là avant mais toutes éparpillées_

--

Le [**Manifeste agile**](http://agilemanifesto.org/) favorise:
- **Les individus et leurs interactions** plus que les processus et les outils
- **Des logiciels opérationnels** plus qu’une documentation exhaustive
- **La collaboration avec les clients** plus que la négociation contractuelle
- **L’adaptation au changement** plus que le suivi d’un plan

notes:
Nous découvrons comment mieux développer des logiciels par la pratique et en aidant les autres à le faire. Ces expériences nous ont amenés à valoriser :

Nous reconnaissons la valeur des seconds éléments, mais privilégions les premiers

-   **Les individus et leurs interactions** plus que les processus et les outils -
-   **Des logiciels opérationnels** plus qu’une documentation exhaustive : L’image doit être bien, ce qui est hors cadre n’a pas d’importance.

En production, travailler signifie généralement livrer un résultat visuel souhaité. Valorisez la capacité de fournir des résultats visuels de manière cohérente. Si le mécanisme est clairement compris, il est reproductible.

-   **La collaboration avec les clients** plus que la négociation contractuelle

Nous travaillons en étroite collaboration avec nos clients. Et lorsque des problèmes surviennent, nous travaillons avec nos clients pour trouver une solution plutôt que de chercher un avocat.

-   **L’adaptation au changement** plus que le suivi d’un plan -

Les retakes artistiques, les changements de réalisateur ou la simplification des plans ne sont pas déterministes.

Bien que ce manifest soit porté sur le logiciel, il est facilement transposable à de nombreuses disciplines.

--

##### Principes agiles

notes:
Ces valeurs ont étés ensuite déclinées en douze principes

--

> Notre plus haute priorité est de satisfaire le client en livrant rapidement et régulièrement des fonctionnalités à grande valeur ajoutée.

notes:
In production, deliver visually significant improvements toward the vision early and often.

--

> Accueillez positivement les changements de besoins, même tard dans le projet. Les processus Agiles exploitent le changement pour donner un avantage compétitif au client.

notes:
Welcome changing vision. Brilliance in the process of movie making often comes from the collaboration itself.

--

> Livrez fréquemment un logiciel opérationnel avec des cycles de quelques semaines à quelques mois et une préférence pour les plus courts.

notes:
Fournissez souvent des résultats visuels. N’attendez pas nécessairement que le résultat parfait soit présenté. La pratique consistant à montrer des quotidiens s’inscrit parfaitement dans ce principe.

--

> Les utilisateurs ou leurs représentants et les développeurs doivent travailler ensemble quotidiennement tout au long du projet.

notes:
Les producteurs, la production et la direction de production travaillent quotidiennement avec les artistes/techs tout au long du projet. Cette collaboration est déjà assez courante, même si ce qu’ils font peut être affiné, clarifié et lui-même mesuré.

--

> Réalisez les projets avec des personnes motivées. Fournissez-leur l’environnement et le soutien dont ils ont besoin et faites-leur confiance pour atteindre les objectifs fixés.

notes:
Faites confiance à ceux qui sont motivés et responsabilisez-les.

--

> La méthode la plus simple et la plus efficace pour transmettre de l’information à l’équipe de développement et à l’intérieur de celle-ci est le dialogue en face à face

notes:
La communication en face à face est la meilleure, que ce soit entre les artistes, les TD, les programmeurs, …

--

> Un logiciel opérationnel est la principale mesure d’avancement.

notes:
Les améliorations visuelles permettent de mesurer les progrès. C’est ce qui rend la production passionnante.

--

> Les processus Agiles encouragent un rythme de développement soutenable. Ensemble, les commanditaires, les développeurs et les utilisateurs devraient être capables de maintenir indéfiniment un rythme constant.

notes:
Keep production to a sustainable slow burn rather than a sequence of exploding fire storms. Firefighting triage should be built into the methods.

--

> Une attention continue à l’excellence technique et à une bonne conception renforce l’Agilité.

notes:
La refonte et la pensée architecturale peuvent s’appliquer au flux de travail des artistes pour améliorer leur agilité.

--

> La simplicité – c’est-à-dire l’art de minimiser la quantité de travail inutile – est essentielle.

notes:
La simplicité augmente l’efficacité du soutien à la vision.

--

> Les meilleures architectures, spécifications et conceptions émergent d’équipes autoorganisées.

notes:
Les meilleures pratiques de production naissent de la collaboration d’une équipe. Une réflexion isolée sur le pipeline peut entraver ce cycle d’amélioration

--

> À intervalles réguliers, l’équipe réfléchit aux moyens de devenir plus efficace, puis règle et modifie son comportement en conséquence.

notes:
En production, faites des rétrospections régulières ; cela arrive, mais pas assez souvent, ou trop tard.

--

Pour résumer…

notes:
Pour résumer,

En ingénierie logicielle, les pratiques agiles mettent en avant la collaboration entre des équipes auto-organisées et pluridisciplinaires et leurs clients.

-   Centré sur l’humain et la communication.
-   Planification adaptative,
-   un développement évolutif,
-   Une livraison précoce et une amélioration continue, et elles encouragent des réponses flexibles au changement.

Les méthodes agiles se veulent plus pragmatiques que les méthodes traditionnelles,

-   impliquent au maximum le demandeur (client)
-   permettent une grande réactivité à ses demandes.

Elles reposent sur :

-   cycle de développement itératif
-   incrémental
-   adaptatif.

**Finalement la méthode agile c’est un peu comme un régime. Et il existe pas mal de recettes pour suivre ce régime !**

--

<img src='attachements/module_4-agile-tribes-subway-map-1-638.jpg' width=800 >

notes:
Voici un petit aperçu des principales recette utilisée pour mettre en place des productions agiles. Chaque couleur représente une recette et les stations sont différents ingrédients.

Comme on peut le constater, certaines recettes utilisent les mêmes ingrédients !

Il n’est pas dans notre intérêt d’aborder TOUTES ces recettes.

Je vais essayer de vous éviter le mal de crâne et aller directement à l’essentiel en abordant une recette qui est utilisé dans notre milieu.

--

![](attachements/module_4-cooking.gif)

notes:
Dans mes recherches sur l’application des méthodes agiles dans notre domaine je suis tombé avec récurrence sur la même recette.

L’idée ça va donc être de voir cette recette dans les grandes ligne et ensuite de voir comment elle est mise en application pour cuisiner des films :)

---

<!-- .slide: data-stack-name="Framework Scrum" -->

## Scrum

notes:
Quelques sites utiles
-   [https://blog.trello.com/fr/methode-agile-scrum-gestion-projet](https://blog.trello.com/fr/methode-agile-scrum-gestion-projet)
-   [https://scrumguides.org/scrum-guide.html](https://scrumguides.org/scrum-guide.html)

On pourrait penser qu’ayant été popularisée par le monde le l’ingénierie logicielle, les recettes des méthodes agile seraient principalement destinées au développeurs et ingénieurs ?

Et non ! Par exemple Scrum à été largement utilisé au FBI, dans des agences de pub ou encore dans le génie civil… Enfait les méthodes Agile et Scrum peuvent s’appliquer dès qu’il y a un produit de n’importe quel type à développé. Les films et les jeux ne font pas exception !

Les fondements de Scrum tournent autour de deux concepts:

--

![](attachements/module_4-xkcd_empirical.png)

[https://xkcd.com/943/](https://xkcd.com/943/)

notes:
**L’empirisme**, qui affirme que la connaissance vient de l’expérience et que de la prise de décisions est basée sur des faits observés.

Le **Lean Thinking** réduit le gaspillage et se focalise sur l’essentiel

--

- Transparence
- Inspection
- Adaptation

notes:
Ces concepts sont cristallisé dans 3 valeurs:

-   Transparence: Chacun doit pouvoir voir ce sur quoi les autres travaillent
-   Inspection: Les artefact et progressions doivent etre inspectée frequement et avec dilligence/efficacité pour trouver les écarts ou problème. L’inspection permet l’adaptation
-   Adaptation: si il y a des aspect du processus qui s’ecarte de la limite acceptable on doit adapter le plus rapidement possible

--

![](attachements/module_4-puzzle.gif)

notes:
On va assembler les pièces qui composent cette méthode ensemble. Hésitez pas si vous avez la moindre question !

--

### Équipe

-   Auto organisées
-   Sans hiérarchies
-   Responsable

notes:
-   Auto organisées: personne n’indique à l’équipe comment travailler sur lees taches
-   Sans hiérarchies: pas de titre
-   Responsable: en cas de problème l’équipes est tenue responsable dans sa globalité

**Et surtout**

--

<img src='attachements/module_4-swiss.jpg' width=500>

notes:
Les équipes sont pluridisciplinaires !

Leur taille max est de 9 personnes, au dela cela deviens compliqué

--

###### Product Owner + Développeurs 

notes:
Le “Product Owner” représente les intérêts du client et à ce titre, il a l’autorité pour définir les fonctionnalités du produit final.

Dans le film ça peut etre le chargé de prod…

Il va être responsable du… -->

--

###### Backlog

notes:
**Backlog**, une liste des tâches et des spécificités du produit (le cahier des charges)

Un point fondamental: le backlog **DOIT** être priorisé. C’est la **responsabilité** du “Product Owner”.

Exemple:

> Si j’utilisais Scrum pour créer une voiture, une de mes tâches prioritaires serait “Doit avoir un moteur”, parce que la voiture ne peut pas fonctionner sans. “Doit être peinte en rouge” serait une tâche moins prioritaire; même si c’est important, ce n’est pas nécessaire pour que la voiture roule

**Faire un schéma du backlog**

C’est l’artéfact principal de la méthode scrum. Il va être utilisé comme source/point de pivot pour gérer les différents évènement !

--

##### Évènements


--

###### Sprint

notes:
C’est le Coeur du réacteur Les Sprints modulent le rythme de Scrum,c’est là où les idées sont transformées en valeur.

Scrum utilise des **sprints** comme intervalles de temps pendant lesquels l’équipe va compléter un certain nombre de tâches du **backlog**. L’intervalle de temps dépend des besoins de l’équipe, mais deux semaines est une durée communément utilisée.

--

###### Plannification de sprint

notes:
Effectué par toute l’équipe

Le but est de:

-   Qu’est-ce qui peut être terminé au cours de ce Sprint ?
-   Comment sera effectué le travail choisi ?

--

###### Daily scrum

notes:
Les équipes se rencontrent tous les jours pour le “Daily Scrum”, une réunion quotidienne où chacun fait part aux autres de son avancement. On appelle également ces réunions des “Daily Stand-Ups”. Ça permet que tout le monde ai connaissance du travail de tout le monde…

1.  On dit ce qu’on a fait le jour d’avant
2.  On dit ce qu’on fera le jour meme
3.  On parle des éventuelles problemes rencontrés

La réunion dure environ 15 minutes.

--

###### Sprint review

notes:
Reunion pendant laquelle on fait l’état des lieux On adapte le backlog, et on prépare les items probable pour le prochain sprint. 

--

Récapitulons

notes:
Finalement l’idée au cœur de la méthodo scrum c’est **l’itération** et **l’amélioration**.

ITERATION:

À la fin d’un sprint le délivrable doit être prêt à être livré au client. Attention le but c’est pas d’avoir un projet fini, loins de là. Mais plutot d’avoir atteint un stade suffisament avancé pour pouvoir montrer quelque chose au client. Dans le cadre d’un film ça peut etre avoir fini l’aspect d’un shot par exemple.

> Si c’était une voiture, ce serait la possibilité de la conduire. Peut-être qu’elle n’aurait pas encore de radio ni de climatisation, mais on pourrait la conduire.

**Pourquoi c’est important ?**

Ça permet de collecter des retours d’expérience utilisateur plus tot pour guider le développement. Ça permet d’avoir un résultat en phase avec les attentes du client.

Ça évite l’un des problèmes courant dans la méthode **waterfall** où le résultat n’est pas aux attentes d’un client. Car au lieu de délivrer seulement le produit final on délivre des petits tançons du produit.

AMELIORATION: Au dela des importances de l’itération, Scrum s’attache également à améliorer le processus en lui même à chaque nouveau cycle. Pendant la rétrospective les équipes discutent des élément qui pourrait les faires gagner en efficience.

Dans le cadre des VFX ça pourrait être des outils qui seraient utiles par exemple.

Le but c’est de focaliser l’équipe sur un but commun : la réalisation du projet. C’est une grande avancée par rapport à d’autres méthodes, qui sont basées sur l’affectation de tâches à des employés (tels des robots) et leur surveillance par leur hiérarchie.

L’objectif est de réussir le travail, pas de permettre à une ou plusieurs personnes de jouer les petits chefs en faisant claquer le fouet au-dessus des têtes.

--

Ça demande plus de travail ?

notes:
Non l’idée c’est de travailler plus intelligement, de gaspiller moins de temps.

Typiquement dans le film on peut souvent passer du temps à attendre un résultat ou une info, et bien ça peux eviter ça entre autre.

Le but c’est de puvoir abbatre plus de travail sans perdre de cheuveux.

--

### Risques

notes:
TODO: parler des risque et problèmes que peuvent apporter les méthodes agiles

[https://www.geek-directeur-technique.com/2009/02/12/Les-methodes-agiles](https://www.geek-directeur-technique.com/2009/02/12/Les-methodes-agiles)

Mettre en place une méthode agile nécessite que tous les acteurs soient impliqués dans le processus, en connaissent les avantages et acceptent de jouer le jeu.

Laisse peu de place à la documentation

Le manque de prévisibilitée ?

--

#### Pour aller plus loin

| <img src='attachements/module_4-scrum_l1.png' width=400 >    |   <img src='attachements/module_4-scrum_l2.png' width=400 >  |
| :---: | :---: |
|  The Art of Doing Twice the Work <br> in Half the Time   |  Le Guide Scrum   |

notes: 
-   **The Art of Doing Twice the Work in Half the Time**: les concepts de scrum expliqués de manière ludique, avec des exemples qui permettent de mieux cerner chaque aspect du framework. Une excellente base.
-   **Le Guide Scrum**: Le guide qui part de zero, parfait pour commencer. (juste 15 pages !)

---

<!-- .slide: data-stack-name="Méthodes agiles dans cinéma d'animation" -->

## Méthodes agiles dans le pré-calculé

--

![](attachements/module_4-recette.jpg)

notes:
On va voir comment le milieu du précalculé à commencé à s’approprié cette recette.

Comme on l’a vu un peu plus tôt les méthodes Agiles se sont initialement popularisée dans le milieu du développement logiciel.

Ce n’est donc pas étonnant que le premier endroit où ces méthodes aient percées dans le pré-calculé soit celui du développement ! De nombreuses entreprise utilisent ces méthodes au sein des équipe R&D pour développer les outils utilisés dans la production !

--

### En R&D ?

--

<img src='attachements/module_4-cube-creative-productions-451113805.jpg' width=300>

notes:
Je peux parler ici d’un exemple concret, à CUBE Creative nous utilisons ça au sein de la RetD !

À quoi ça ressemble ?

--
<!-- .slide: data-transition="slide-in none" -->

![](attachements/module_4-retd_conf.svg)

notes:
Et bien il faut imaginer la R&D comme une equipe à scrum qui traitera avec ses clients qui sont la production et la supervision.

--

<!-- .slide: data-transition="none slide-out" -->
![](attachements/module_4-retd_conf2.svg)

notes:
Par exemple ici la prod et les superviseurs formulent chacun un besoin, une demande.

--

![](attachements/module_4-cube_backlog.png)

Backlog utilisé à cube.

notes:
Les clients rentre leur demande dans le système de ticket de gitlab.

SI vous vous souvenez, c’est ça qu’on a utilisé pour mener le TP pipe temps réel :)

--
<!-- .slide: data-transition="slide-in none" -->

![](attachements/module_4-retd_conf3.svg)
notes:
Dans notre cas le “product owner” serait le lead RetD (valentin moriceau). Il joue le role de tampon entre les clients et les ingénieurs de la reud.

Il est garant du backlog et de la priorisation des tâches et va avoir la vision d’ensemble.

--
<!-- .slide: data-transition="none" -->

![](attachements/module_4-retd_conf4.svg)

notes:
Chaque semaine les membres de l’équipes discutent collégialement de l’assignation des tâches le lundi lord de la plannification des sprints

Les sprints durent environs deux semaines. Durant ces deux semaines chacun travail sur des tâches assignées au début du sprint (+ les galères de pipe qui peuvent beaucoup plomber, Elaborer cet aspect)

Chaque jour, durant les matins debout chacun fait état de ce qu’il à fait le jour d’avant et de ce qu’il fera le jour d’après.

--
<!-- .slide: data-transition="none" -->

![](attachements/module_4-retd_conf5.svg)

notes:
Répartition

--
<!-- .slide: data-transition="none" -->

![](attachements/module_4-retd_conf6.svg)

notes:
Les développeurs qui travaillent sur les tâches communiquent ensuite directement avec les clients dans les tickets pour:

-   preciser les besoins
-   mener des tests,…

À la fin d’un sprint on à une réunion qu’on nomme le “spectacle” où les élément de la reud présentent les fonctionnalités et ou outils qu’ils sont développé aux clients (prod/supervision).

Cet évenement se déroule en deux étapes:

1.  Presentation des étapes
2.  Retours et demandes des clients
3.  Ajustement du “backlog”

La seconde étape est vitale pour prioriser et ajuster les différentes tâches en cours au besoin. Ensuite on repart pour un sprint de deux semaines.


--

### En production ?

notes:
Autant il est vrai que les équipe R&D ont rapidement changé de modèle pour les méthodes agiles, autant les production ont du mal à s’y mettre.

Alors comment utiliser les méthodes agile pour gérer la création artistique de film ?

Dans la majorité des productions actuelles, le modèle de gestion en tu temps consiste à TOUT planifier en pré production puis ajuster la date limite de livraison…

Souvent lorsqu’on est contraint par les deadlines a faire des heures supplémentaire on associe ça à une mauvaise gestion de la production. Mais le management n’échape pas à la règle qu’avec les mauvais outils il est quasiment impossible de réussir à faire une tache donnée…

--

![](attachements/module_4-gantt_vfxguide.jpg)

Gantt pour une production “waterfall” classique, _fxguide_

notes:
Le problème de ce genre planification c’est que les estimations sont plus compliquée à mener.

En effet, on cherche à estimer la durée de tâches “complexes”.

Or plus les tâches seront grosses plus on gaspillera du temps entre les tâches.

-   Les dépendances ne sont pas claires tant que le système n’est pas défaillant.
-   Il suppose qu’une estimation précise peut être faite pendant la durée de vie du projet.
-   C’est un processus descendant où la gestion et la nature même de la gestion du projet est une cascade dont les décisions et les actions descendent d’un niveau à l’autre. Il peut donc manquer d’adhésion de la part des membres de l’équipe.

--

![](attachements/module_4-productionfire.gif)

Un chargé de prod dans une organisation “waterfall”

notes:
Finalement les chargés de prod se retrouvent à jouer le role de pompier pour éteindre les feux qui se déclenché à gauche et a droite…

Comment éviter de tomber dans ce cercle vicieux ?

Et bien les méthodes agiles peuvent aider à envisager les choses autrement !

--


#### Étude de cas

![](attachements/module_4-fxsrc.svg)

notes:
Jusqu’a maintenant les recherches sur l’utilisation des méhtodes agiles dans les VFX sont quasiment inexistantes. Pour trouver des informations j’ai du creuser sur des cas réel ! Il n’y en avais pas beaucoup mais il y en avait tout de meme. C’est là qu’on voit qu’on est pas encore dans une appropriation des méthodes par le secteur.

Cependant c’est le Turfu :3

Pour cette partie je me suis donc principalement basée sur les expériences de trois différents studios:

-   Cinnamon VFX un studio de FX ukrainien [vfx]
-   Main Road Post un studio de FX russe [vfx] 2100 shot
-   Gwenaelle Dupre de CGWire [Serie d’animation]

Dans les trois cas, l’adoption des méthodes agile c’est fait par nécessité:

-   pour cinamon: ils ont fondé le studio dans le but d’explorer de nouvelles façons de produire, d’être basé sur un idéal.
-   pour main road post:
    -   Le studio à démarré par une équipe de 6, tout le monde communiquai
    -   Puis il sont devenu 50… ils ont alors mis en place des départements…
        -   C’est devenu compliqué à cause du phenomene de silo.
-   pour Gwenaelle:
    -   Elle à mis en place la méthode pour éteindre l’incendie qui avais bien débuté

Finalement ce que j’ai essayé c’est de synthétiser ces trois retours d’expérience afin de vous donner une idée concrète d’application de l’agile en production

--

#### Structure des équipes

--

![](attachements/module_4-hierachy.svg)

Une organisation hiérarchique classique.

notes:
MRP:

-   Mauvaise communication entre départements
-   Les superviseurs représentent un goulot d’étranglement dans le processus de review (pas de retour rapides)
-   Les Gantt ne marchent jamais car ils ne sont pas précis
-   Les planning etaient toujours trop optimistes, crush times
-   EFFET SILO (chacun travail dans son coin)

--

### Inter-disciplinaires vs Spécialisés

notes:
Lorsqu’on commence à imaginer la possibilités de faire des scrums team, une question viens directement, comment construire ses équipes ?

Dans les différent mise en application, deux pattern d’equipes ressortent:

-   interdisciplinaires: Suivre a fond l’idéologie agile en cassant complètement les département
-   spétialisée: mettre les pieds à moitié dans le plat

Pour citer le guide :

> Les Équipes Scrum (Scrum Teams) sont auto-organisées et pluridisciplinaires.

Il faut savoir que cela dépendra énormément du type de projets,

**[MRP] - VFX**

Dans le cas des VFX [effets spetiaux] il est plus simple d’avoir une équipe cross disciplinaire de par la nature moins diversifiée des taches mené dans la production.

**[CGWire] - Animation**

Dans le cadre d’une série d’animation c’est plus compliqué. Durant son experience Guenaelle à mis en place des équipe spétialisés.

il y a plus de métiers… Cela ferait dont des équipes plus grosses.

**Dans tout les cas, les petites équipes sont privilégiés**

Elle sont également auto-gérée

--

![](attachements/module_4-structure_interdisiplinaire.svg)

Organisation des équipes interdisciplinaires en place à Main Road Post.

notes:
1.  Cycle de travail court
2.  Équipes pluridisciplinaires (plus de départements). ! cela ne signifie pas que tout le monde est generaliste dans l’équipe. Cela signifie que c’est une somme de spécialites
3.  Les équipes décident comment elle vont faire leur travail,
4.  Les équipes décident comment elles se répartissent les tâches. Il n’y a pas de superviseur assignant les tâches.
5.  Les équipes décident du recrutement
6.  les équipes sont responsables du résultat final
7.  Le réalisateur va voir chaque équipes

90 personnes, 13 équipes differentes, Gateway (zone tampon avec des “freelancer”, si quelqu’un se fait virer d’une équipe par exemple)

Chaques equipes et comme un petit studio dans le studio

C’est comme un organisme vivant, c’est plus une machinerie

Comment le pipe est developpé ?

-   Chaque equipe fait sa popote

Les gens commencent a sourir, parler,… Essayer de travailler en etant plus heureux, en respectant une balance.

--

![](attachements/module_4-structure_spetialise.svg)

Organisation des equipes spétialisés mises en place par Gwenaelle Dupre.

notes:
Les équipes étaient composée de Senior, Mid et Juniors. Cela permet d’avoir un niveau global correct et de ne pas laisser les juniors derrière.

Un representant à été selectionné dans chaque équipe. Le superviseur de l’étage dialoguerai principalement avec ce représentant plutôt qu’avec l’equipe

--

#### Sprints

notes:
Comment marche le coeur du réacteur ?

--

![](attachements/module_4-Scrum-830x460.jpg)

Rythme de la production Agile à Cinamon, fxguide

notes:
Finalement la production se transmorme en une succession de sprint.

Dans les trois cas que j’ai observé, les équipes coordonnaient leur sprint de manière similaire en trois phases

1.  sprint planning - Chaque début de sprint, les équipes choisissent leur todo
2.  daily - Tout les jour
3.  Retrospective

La durée des sprints dépend vraiment du type de production:

-   [MTP] -Sprints cours (pas de durée précisée)
-   [CGWIRE] Sprint de 1 semaine. Il etaient partis sur 6 semaines aux début, perte de memoire. Faire des cycles cours permettait de se souvenir du brief du realisateur
-   [CINAMON] Entre 1 et 4 semaine selon le type de projet.

En fonction de la structure des équipes les sprints sont dépendant les uns des autres.

-   Dans le cas de MTP et CINAMON les sprints sont indépendants
-   Dans le cas de Gwenaelle les sprints sont necessairement dependant

On va voir un peu plus en détail comment peuvent s’organniser les choses…

--

#### Tâches


--

![](attachements/module_4-complexvsSimple-830x475.jpg)

complex vs simple, _fxguide_.

notes:
**[CINAMON]**

L’idée en premier lieu c’est de revoir l’échelle de temps. Découper les choses à faire en tâches réalisable en une journée permettra de faire des prévisions plus précises et de moins perdre de temps.

il est plus facile de prévoir 1000 petites tâche qu’un seul grande

C’est un peu comme ça que le GPS de votre voiture fonctionne… ^^

Pour la meme raison il est egalement dur d’avoir une vision du temps restant sur une tache. Le modele actuelle peut dire si quelqu’un travail maintenant sur une tache mais ses estimations seront souvent fausses.

--

![](attachements/module_4-board-830x509.jpg)

Le kanban board utilisé à Cinamon Fx.

notes:
Que ce soit Cinamon, MRP ou Gwenaelle, ils ont tous utilisé le meme outils pour gérer leur tache: un kanban board.

Kanban qui signifie etiquette en japonai, c’est un outil de inventé par Toyota dans les années 60 pour gérer la production avec la methode Kanban. Ça à bien évolué depuis mais le concept est le même, c’est un tableau composé de colonnes qui montrent un état et de cartes qui représentent les tâches.

Il résume dont:

-   Qui fait la tache ?
-   le status la tache ?

Toute les petites tache qu’on a vu juste avant sont organisée/ classée/ priorisée via ce tableau.

Durant chaque sprints, les équipes mettent à jour leur tache sur le kanban partagé.

Cela rend le traacking de la progression facile !

--

#### Priorisation

![](attachements/module_4-pocker2.gif)

notes:
Lorsqu’on parlais de scrum, nous avions dit qu’il était VITALE de prioriser les taches durant la plannification de chaque sprint. Dans les trois cas étudié, les équipes estiment combien de temps les ptite tâches prendrons.

Mais ici seul CINAMON à parlé de sa méthode qui m’a parru géniale;

**"Le sprint pocker"**

La plannification se fait en équipe avec des personnes votant sur la complexité des Tâches.

Les seules options de vote sont 0,1,2,3,5,8,13,21,34 Infinity…

l’idée est qu’au fur et à mesure que la tâche grandit, l’erreur d’estimation augmente également.

Il est donc impossible qu’au dela d’un jour ou deux une estimation soit précise. Dire qu’une tâche prendra 19,5 jours implique un niveau de précision qui n’existe tout simplement pas. Ce procédé présente de nombreux avantages:

Points positifs:

-   les estimations n’impliquent pas l’exactitude qu’elles n’ont pas
-   les membres de l’équipe adhèrent - ils «possèdent» leurs propres estimations
-   si une personne estime très différemment - alors peut-être qu’elle sait quelque chose que d’autres ignorent
-   au fur et à mesure que le processus est répété et revu par Sprint - l’équipe apprend et améliore rapidement les estimations

Une fois les taches correctment priorités, il est possible de commencer le sprint.

--

#### Daily

notes:
Rappeler le principe du daily. Chaque jour les équipe mettent à jour les taches sur le kanboard lors du daily.

**CGWIRE**

Au début il y a un facteur de “honte”.

A la fin de chaque daily, le representant de la STeam met a jour le status des cartes de sont equipe

Cette configuration à de gros avantage pour le superviseur qui n’a plus besoin d’aller voir chaque artistes pour leur demander où ils en sont. Il à juste a regarder le tableau.

Il était alors possible de plannifier le future plutot que de se focaliser sur le passé

--

#### Burndown charts

![](attachements/module_4-burndown-1-830x453.jpg)

Tracking temporelle avec les _Burndown Charts_, fxguide.

notes:
Durant les sprints, les Burndown Charts permettent de voir et d’anticiper les retards/avances. Et surtout de réagir (montrer le graphique central)

--

#### Overlap des projets

--

![](attachements/module_4-overlap_vfxguide.jpg)

Ce qui peut arriver avec les méthodes traditionnelles
 
notes:
Lorsqu’on se base sur les méthodes classique en plannifiant tout le projet sur le long terme, il devient compliquer d’optimiser la charge sur plusieurs projets simultanément

--

![](attachements/module_4-fmx_charts04-830x532.jpg)

Les règles d’overlap chez Cinamon, _fxguide_.

notes:
Les sprint étant une plus petite unité de temps, cela leur a permis de mieux gérer l'overlap de projet en repartissant mieux les charges à travers le temps.

Le chemin d’Agile est une réaction au projet, pas un projet suivant un chemin prescrit. Il ne prétend pas qu’il y a un plan directeur à suivre, il suppose de petites étapes avec des révisions et des contrôles constants, il suppose l’implication de chacun dans une mise en œuvre en évolution.

--

![](attachements/module_4-suspens.gif)

notes:
Du coup je vous ai a peu près tout dit sur ce que j’avais vu qui avait été exploré en production.

Cependant un certains nombre de questions restent en suspend… Comme par exemple:

--

#### Gérer les retakes

notes:
Les retakes doivent être prises en charge par l’ensemble de l’équipe et non la personne qui est à l’origine. Ça permet:

-   d’avoir des regards nouveaux sur le travail, ce qui augmente la qualité globale
-   aux juniors de pas passer trop de temps sur leur shot
-   d’avoir un meilleur processus d’apprentissage, les seniors vont dire aux junior ce qui cloche

--

#### Comment s’intègre la R&D ?
notes:
En étant une équipe scrum à part entière ça pourrai marcher !

Si chaque équipe est un client de la reud…

--

#### Flow efficiency VS Ressource efficiency

notes:
Waterfall == Ressource efficiency

[https://www.jrothman.com/mpd/agile/2015/09/resource-efficiency-vs-flow-efficiency-part-1-seeing-your-system/](https://www.jrothman.com/mpd/agile/2015/09/resource-efficiency-vs-flow-efficiency-part-1-seeing-your-system/) [http://www.agilevfx.com/production/mindset/](http://www.agilevfx.com/production/mindset/)

--

### Outils

--

![](attachements/module_4-agile_vfx_apps.svg)

notes:
! adopter scrum demande beaucoup d’effort et une restructuration !

--

<img src='attachements/module_4-shotty_board.png' width=1200>

---
<!-- .slide: data-stack-name="Aller plus loins" -->

## Aller plus loin

--


### Agile & Pré-calculé

--

#### Publications

-   Bethell, Dan. “**Into the Wasteland: The VFX of Mad Max : Fury Road**” In Proceedings of the 2015 Symposium on Digital Production - DigiPro ’15, 57–57. Los Angeles, California: ACM Press, 2015. [https://doi.org/10.1145/2791261.2791275](https://doi.org/10.1145/2791261.2791275).
-   Seymour, Mike, and Sharon Coyle. “**Towards a Research Agenda for Adopting Agile Project Management in Creative Industries**” 2016, 11.

--

#### Sites

-   [Agile atMain Road Post](https://youtu.be/NpvWOuxXVhg)
-   [Agile at Cinnamon VFX](https://www.fxguide.com/fxfeatured/scrum-in-vfx/)
-   [How To Apply The Scrum Methodology To a Cartoon TV Show Production](https://medium.com/cgwire/how-to-apply-the-scrum-method-to-a-cartoon-tv-show-production-bc09c72e40b0)
-   [CGWire Agile CG Pipeline](https://medium.com/cgwire/agile-cg-pipeline-3005f5f7f7f1)
-   [Agile Visual Effects and Feature Animation](http://www.agilevfx.com/)

--

### Agile et temps réel

--

#### Publications

-   Petrillo, Fabio, and Marcelo Pimenta. “**Is Agility out There ? Agile Practices in Game Development**” 9–15, 2010. [https://doi.org/10.1145/1878450.1878453](https://doi.org/10.1145/1878450.1878453).
-   Musil, Juergen, Angelika Schweda, Dietmar Winkler, and Stefan Biffl. “**Improving Video Game Development: Facilitating Heterogeneous Team Collaboration through Flexible Software Processes** ” In Systems, Software and Services Process Improvement, edited by Andreas Riel, Rory O’Connor, Serge Tichkiewitch, and Richard Messnarz, 99:83–94. Communications in Computer and Information Science. Berlin, Heidelberg: Springer Berlin Heidelberg, 2010. [https://doi.org/10.1007/978-3-642-15666-3_8](https://doi.org/10.1007/978-3-642-15666-3_8).
-   Ramadan, Rido, and Yani Widyani. “**Game Development Life Cycle Guidelines**” In 2013 International Conference on Advanced Computer Science and Information Systems (ICACSIS), 95–100. Sanur Bali, Indonesia: IEEE, 2013. [https://doi.org/10.1109/ICACSIS.2013.6761558](https://doi.org/10.1109/ICACSIS.2013.6761558).
-   Murphy-Hill, Emerson, Thomas Zimmermann, and Nachiappan Nagappan. “**Cowboys, Ankle Sprains, and Keepers of Quality: How Is Video Game Development Different from Software Development?** ” In Proceedings of the 36th International Conference on Software Engineering - ICSE 2014, 1–11. Hyderabad, India: ACM Press, 2014. [https://doi.org/10.1145/2568225.2568226](https://doi.org/10.1145/2568225.2568226).

--

#### Sites

-   [Game Production Cycle](https://leonardperez.net/game-production-cycle/)
-   [Game Design With Agile Methodologies](https://www.gamasutra.com/view/feature/131151/paper_burns_game_design_with_.php)

--

## Des questions ?