# Collaboration en temps réel pour la création 3D

---

## Objectif des modules 

- Module 1 : Pipeline : rappels sur les fondamentaux<!-- .element: class="fragment" -->
- Module 2 : Collaboration en temps réel quésako ?<!-- .element: class="fragment" -->
- Module 3 : Projet de groupe : créer un environment 3D de zéro en groupe<!-- .element: class="fragment" -->

--
<!-- .slide: data-name="Module 1" -->

### Module 1 : Pipeline : rappels sur les fondamentaux
.
1. Comprendre comment se structure un pipeline<!-- .element: class="fragment" -->
2. Avoir des éléments de vocabulaire pour l'entreprise<!-- .element: class="fragment" -->
3. Connaitre les mécanismes primitifs qui sont également utilisés pour la collaboration en temps réel<!-- .element: class="fragment" -->

--
<!-- .slide: data-name="Module 2" -->

### Module 2 : Collaboration en temps réel quésako ?

1. Visualiser où peut s'intégrer la collaboration en temps réel<!-- .element: class="fragment" -->
2. Connaitre un workflow pour collaborer en temps réel<!-- .element: class="fragment" -->
3. Faire un tour d'horizon des solutions<!-- .element: class="fragment" -->

--
 
![module_0-se_faire_les_armes.jpg](attachements/module_0-se_faire_les_armes.jpg)

--

### Module 3 :  Projet de groupe : créer un environment de zéro 


![](attachements/module_0-training.jpg)

*Pratiquer, pratiquer, pratiquer !*


---

## Des questions ?

