
# Pipeline pour les productions pré-calculées

---

## Précédemment

-   Aspects fondamentaux d'un pipeline<!-- .element: class="fragment" -->
    -   Enjeux<!-- .element: class="fragment" -->
    -   Composants<!-- .element: class="fragment" -->
-   Workflow temps réel / précalculé<!-- .element: class="fragment" -->

notes:
Précédemment on à vu :
- Aspects fondamentaux d'un pipeline 
- Enjeux 
- Composants 
- Workflow temps réel / précalculé

--

![](attachements/module_2-animation_workflow6%201.svg)
Un workflow classique pour la fabrication de film.

--

![](attachements/module_2-chain.gif)

notes:
Nous avions constaté que dans le cadre du film d'animation, presque tout était séquentiel Mais j'avais volontairement ignorée un aspect crucial… 
Mais nous avions pas discuté de ce qui reliais les différentes étapes du workflow: j'avais volontairement hormis de parler de dataflow

---

<!-- .slide: data-stack-name="Workflow & Dataflow" -->

### Workflow/Dataflow, Une relation étroite

--

<img src='attachements/module_2-house.jpg' width=500/>

notes:
L'un n'existe pas sans l'autre

L'un supporte l'autre 

En fonction des matériaux que vous choisissez pour construire votre maison, le résultat sera différent.

Finalement au début on peut commencer par trouver un workflow correspondant aux besoin et tenter ensuite de trouver un dataflow correspondant.

--

-   Nature du projet<!-- .element: class="fragment" -->
-   DCC<!-- .element: class="fragment" -->
-   Maturité du pipeline<!-- .element: class="fragment" -->

notes:
Concrètement le dataflow est directement lié à différents paramètres : 
- À la nature du projet (serie/film/pub/..) 
- DCC choisis pour effectué les étapes de prod 
- Au niveau de maturation  du studio (niveau de pérennisation des outils), À la taille du studio (besoins differents) 

Le choix du dataflow impactera beaucoup le workflow sus-jacent, il déterminera : 
- Sa rigidité décisionnelle (la possibilité de modifier des choses dans les précédentes etapes...) 
- Un peu tout finalement... On va donc voir comment peut émerger le dataflow d'un projet d'animation !

---

### Cas concret : Production d'une série animée

Inspiré de fait réels…

notes:
Cas concret Imaginons devoir produire une série animée PAR EXEMPLE

--
<!-- .slide: data-transition="slide-in none" -->
![](attachements/module_2-explore.gif)

notes:
En général les superviseurs (et ou R&D) commencent évalues quelles pourraient êtres les meilleurs manière/outils pour répondre aux spécificités de la production (defis techniques etc…)

--
<!-- .slide: data-transition="none" -->

![](attachements/module_2-animation_workflow_nodes.svg)
notes:
De part le savoir faire du studio et l'avis des superviseurs et de la R&D il a été décidé de procéder de la manière suivante. On se focalisera principalement sur le dataflow de la PRODUCTION.

--
<!-- .slide: data-transition="none" -->

![](attachements/module_2-animation_workflow_nodes_1.svg)
notes:
On divise la création d'asset en trois grandes catégories

--
<!-- .slide: data-transition="none" -->

![](attachements/module_2-animation_workflow_nodes_2.svg)
notes:
Les personnages

--
<!-- .slide: data-transition="none" -->

![](attachements/module_2-animation_workflow_nodes_3.svg)
notes:
Les props Ces deux derniers on la même chaine de production, Shading(+Texturing) = Texture creation - bank

--
<!-- .slide: data-transition="none" -->

![](attachements/module_2-animation_workflow_nodes_4.svg)

notes:
Les décors qui on besoin des props pour être mis en place 

- Assembly = assemblage des pieces de la bank, une sorte de layout finalement
- Scaterring = Herbes, ... 
- Lighting = Moods (night/morning/day)

--
<!-- .slide: data-transition="none" -->

![](attachements/module_2-animation_workflow_nodes_15.svg)
notes:
Les dcc ont donc été choisis de part: 
- le contexte technique (solutions deja en places dans le studio) 
- les tests techniques menés par les superviseurs sur certains aspect critiques (rendu, rigs,...) sur le pilote notamment 

Maintenant on va aller un peu plus en profondeur et essayer de voir comment les éléments transitent entre les tâches ! \o/

--
<!-- .slide: data-transition="none" -->

![](attachements/module_2-animation_workflow_nodes_16.svg)

notes:
En général, lors des transitions internes à une tâches, le format des données va dépendre des dcc associés à la taches

--
<!-- .slide: data-transition="none" -->

![](attachements/module_2-animation_workflow_nodes_17.svg)

notes:
Donc lors de l'asset construction par exemple on utilise blender pour toute les étape de la construction d'asset, on a donc priviliégié les `.blend` 

Pareil pour le layout et l'animation 

La phase d'animatic est un peu a part (outils maisons, process avec l'asset manager)

--
<!-- .slide: data-transition="none" -->

![](attachements/module_2-animation_workflow_nodes_18.svg)

notes:
Parler des possibilités qu'offrent le fait de partager le même format entre plusieurs étapes de la fab.

--
<!-- .slide: data-transition="none" -->

![](attachements/module_2-animation_workflow_nodes_19.svg)

notes:
À partir de l'animation on va commencer à **cacher**/**figer** les choses et plus particulièrement les géométries

--
<!-- .slide: data-transition="none" -->

![](attachements/module_2-animation_workflow_nodes_20.svg)
notes:
Pareil pour les vfx, cache alembic Commencer a introduire le faire que les standart sont les fondations d'échange… (houdini/blender !)

--
<!-- .slide: data-transition="none" -->

![](attachements/module_2-animation_workflow_nodes_21.svg)
notes:
Ici on démarre l'export en EXR

--
<!-- .slide: data-transition="none slide-out" -->

![](attachements/module_2-animation_workflow_nodes_22.svg)

notes:
Enfin on exporte des séquences d'images pour la post production.

On constate donc que le choix de ces DCC à directement impacté les formats de fichiers utilisés. C'est aussi en fonction des besoins du workflow (faut-il figer les décisions à un certains moments en utilisant des caches ? ) Toujours en fonction de ces besoins, on essayera d'utiliser des **standards**

---

![](attachements/module_2-standard-young.gif)

notes:
Bien qu'on produise nos films comme des voitures à l'époque du fordisme, Il faut savoir que l'industrie du cinéma d'animation est encore relativement jeune… 
Il n'y a donc pas encore de standard couvrant tous les aspects(définir aspect) du dataflow.

Cependant il en existe tout de meme des biens utiles, je vais donc vous faire un tour d'horizon :)

---
<!-- .slide: data-stack-name="Standards pour collaborer" -->

## Standards pour collaborer

sur un projet pré-calculé.

notes:
L'idée de ce chapitre ça va être de vous faire un format paysage des formats standard utilisé en productions. Ça vous permettra également de vous aider à derterminer vos dataflow durant les intensifs :innocent:  
Les standards facilitent l'échange entre studios !

--

<img src='attachements/module_2-standard-presented.svg' width=1000>

Quelques standards d'échanges pour le pré-calculé.

notes:
Voici ceux que l'on va parcourir. L'idée ça va être de faire un tour d'horizon en suivant l'ordre d'une chaine de production.

--

### Types de standards

- **Cache**: Résultat Figé 
- **Travail**: Loseless / Procédural

--
![](attachements/module_2-alembic_features.png)
notes:
ALEMBIC Dévelopé par Sony Image Works et ILM Supporte les representations geométriques classiques dans l'industrie: 
- polygon meshes 
- subdivision surface 
- parametric curves 
- NURBS patches 
- les particules. 
Il supporte aussi: 
- les hiérarchies de transformation 
- les cameras 
- les materiaux 
- les lights 

Comme les image par rapport au lighting et au rendu non-procedural il permet de distiller les résultats des disciplines artistiques pour les transmettre à d'autres artistes dans d'autres disciplines.

--

![](attachements/module_2-feature-openvdb.png)
notes:
OpenVDB Développé par DreamWorks Animation OpenVDB represents high-resolution sparse volumes = Volumes ! Souvents utilisé pour stocker des simultation de VFX Efficace

--
<img src='attachements/module_2-gltf.png' width=400>

![](attachements/module_2-feature-gltf.png)

--

Exemple de fichiers GLTF

```json
{
	"asset": {
		"generator": "COLLADA2GLTF",
		"version": "2.0"
	},
	"scene": 0,
	"scenes": [
		{
			"nodes": [
				0
			]
		}
	],
	"nodes": [
		{
			"children": [
				1
			],
			"matrix": [
				1.0,
				0.0,
				0.0,
				0.0,
				0.0,
				0.0,
				-1.0,
				0.0,
				0.0,
				1.0,
				0.0,
				0.0,
				0.0,
				0.0,
				0.0,
				1.0
			]
		},
		{
			"mesh": 0
		}
	],
	"meshes": [
		{
			"primitives": [
				{
					"attributes": {
						"NORMAL": 1,
						"POSITION": 2
					},
					"indices": 0,
					"mode": 4,
					"material": 0
				}
			],
			"name": "Mesh"
		}
	],
	"accessors": [
		{
			"bufferView": 0,
			"byteOffset": 0,
			"componentType": 5123,
			"count": 36,
			"max": [
				23
			],
			"min": [
				0
			],
			"type": "SCALAR"
		},
		{
			"bufferView": 1,
			"byteOffset": 0,
			"componentType": 5126,
			"count": 24,
			"max": [
				1.0,
				1.0,
				1.0
			],
			"min": [
				-1.0,
				-1.0,
				-1.0
			],
			"type": "VEC3"
		},
		{
			"bufferView": 1,
			"byteOffset": 288,
			"componentType": 5126,
			"count": 24,
			"max": [
				0.5,
				0.5,
				0.5
			],
			"min": [
				-0.5,
				-0.5,
				-0.5
			],
			"type": "VEC3"
		}
	],
	"materials": [
		{
			"pbrMetallicRoughness": {
				"baseColorFactor": [
					0.800000011920929,
					0.0,
					0.0,
					1.0
				],
				"metallicFactor": 0.0
			},
			"name": "Red"
		}
	],
	"bufferViews": [
		{
			"buffer": 0,
			"byteOffset": 576,
			"byteLength": 72,
			"target": 34963
		},
		{
			"buffer": 0,
			"byteOffset": 0,
			"byteLength": 576,
			"byteStride": 12,
			"target": 34962
		}
	],
	"buffers": [
		{
			"byteLength": 648,
			"uri": "Box0.bin"
		}
	]
}
```

--

<img src='attachements/module_2-usdlogo.png' width=600>

![](attachements/module_2-feature-usd.png)

--

![](attachements/module_2-usd-ring.jpg)

notes:
L'USD c'est un peu l'anneau pour les gouverner tous ^^

--

![](attachements/module_2-MaterialX.png)

![](attachements/module_2-feature-materialX.png)
notes:
MaterialX Developpé par Lucasfilm, utilisé à ILM sur starwars notement

--

Exemple de code materialX
```xml
<?xml version="1.0" encoding="UTF-8"?>
<materialx version="1.37" cms="ocio" colorspace="lin_rec709">
	<nodedef name="ND_simple_srf_surface" node="simple_srf">
		<input name="diffColor" type="color3" value="0.18, 0.18, 0.18"/>
		<input name="specColor" type="color3" value="0.05, 0.05, 0.05"/>
		<input name="specRoughness" type="float" value="0.25"/>
		<output name="out" type="surfaceshader"/>
	</nodedef>
	<implementation name="IM_simple_srf" nodedef="ND_simple_srf_surface" />
</materialx>
```

--

![](attachements/module_2-osl.png)

![](attachements/module_2-feature-osl.png)

notes:
OSL (Open Shading Language) = glsl du précalculé Développé par Sony Pictures Imageworks

--

Exemple de fichier OSL :
```c
shader SimpleMaterial(
	color diffuse_color = color(0.6, 0.8, 0.6),
	float noise_factor = 0.5,
	output closure color bsdf = diffuse(N))
	{
		color material_color = diffuse_color * mix(1.0, noise(P * 10.0), noise_factor);
		bsdf = material_color * diffuse(N);
	}
```

![](attachements/module_2-osl_demo.png)<!-- .element: class="fragment" -->

notes:
Syntaxe du C Open Shading Language (OSL) is a small but rich language for programmable shading in advanced renderers and other applications, ideal for describing materials, lights, displacement, and pattern generation

--

![](attachements/module_2-ptex.png)

![](attachements/module_2-feature_PTEX.png)

notes:
Développé par waltDisney Ptex is a **texture mapping system** developed by Walt Disney Animation Studios for production-quality rendering: No UV assignment is required ! Ptex applies a separate texture to each face of a subdivision or polygon mesh. The Ptex file format can efficiently store hundreds of thousands of texture images in a single file. The Ptex API provides cached file I/O and high-quality filtering - everything that is needed to easily add Ptex support to a production-quality renderer or texture authoring application.

--

<img src='attachements/module_2-OpenEXR.svg' width=300>

![](attachements/module_2-feature-OPENEXR.png)

notes:
OpenEXR Dévelopé par ILM 2D data only The purpose of format is to accurately and efficiently represent high-dynamic-range scene-linear image data and associated metadata, with strong support for multi-part, multi-channel use cases 
- High dynamic range and color precision. 
- Support for 16-bit floating-point, 32-bit floating-point, and 32-bit integer pixels. 
- Multiple image compression algorithms, both lossless and lossy. 
- Extensibility. 
- Support for stereoscopic image workflows and a generalization to multi-views. 
- Flexible support for deep data: pixels can store a variable-length list of samples and, thus, it is possible to store multiple values at different depths for each pixel. Hard surfaces and volumetric data representations are accommodated. 
- Multipart: ability to encode separate, but related, images in one file. This allows for access to individual parts without the need to read other parts in the file. 

**Exemple avec les Cryptomates.

--

<img src='attachements/module_2-OpenTimelineIO.png' width=600>

![](attachements/module_2-feature-OTIO.png)

--

![](attachements/module_2-otioview.png)

--

#### Feature par standard

<img src='attachements/module_2-dcc_standards_features.svg' width=1300>
--

#### Compatibilité DCC /standards


[<img src='attachements/module_2-dcc_standards.svg' width=1300>](https://docs.google.com/spreadsheets/d/1-XJscWD-hLPsLMFkxQszHUc6jAfhNpEJHmcLqpYf_Aw/edit?usp=sharing)

--

#### Compatibilité standards/standards

[![](attachements/module_2-vfxreference.jpg)](http://vfxplatform.com/)
 
La plateforme de référence pour les VFX, le standard des standards.

notes:
Reference Platform is updated annually by a group of software vendors in collaboration with the Visual Effects Society Technology Committee. C'est une norme pour permettre aux outils de création d'être **COMPATIBLE** entre eux. Si vous êtes amené à développer des outils pipe ou autre, vous devriez vous y référer ! Example de blender qui a fait n'imp pendant des années, résultat faire un pipe pour intégrer blender était compliqué à cause des versions différentes de Python qu'il utilise… Exemple à CUBE


---


## Des questions ?


