# Pipeline de production pour l'industrie du film d'animation et du jeu vidéo

Un cours en six étapes:

1. [Introduction générale](1_introduction_generale/index.html)
2. [Pipeline pour le pré calculé](2_pipeline_precalcule/index.html)
3. [Pipeline pour le temps réel](3_pipeline_temps_reel/index.html)
4. [Le rôle de l'automatisation](4_automatisation/index.html)
5. [Méthode agiles](5_methodes_agiles/index.html)
6. [Méthode émergente: la collaboration en temps réel](6_collaboration_en_temps_reel/index.html)